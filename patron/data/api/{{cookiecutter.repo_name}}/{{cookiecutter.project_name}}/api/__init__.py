# -*- coding: utf-8 -*-
from flask.blueprints import Blueprint

api = Blueprint('api', __name__)

__all__ = ['api']
